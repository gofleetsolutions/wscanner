# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
@package test_wscanner

This package implements the wscanner unit tests
"""

import sys
import unittest
from unittest.mock import patch

from wscanner.main import parse_args

DEFAULT_ARGUMENTS = {'interface': 'mon0',
                     'period': 10,
                     'beacons_only': False,
                     'probes_only': False,
                     'quiet': False,
                     'vendors_path': None,
                     'address': None,
                     'topic': 'WLS',
                     'show_version': False}


class TCWscanner(unittest.TestCase):
    """ Implements unit tests for the wscanner
    """
    def test_parse_args(self):
        """ Check the arguments parser
        """
        testargs = ["wscanner"]
        with patch.object(sys, 'argv', testargs):
            arguments = parse_args()

            self.assertEqual(arguments, DEFAULT_ARGUMENTS)
