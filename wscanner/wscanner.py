# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
Wireless scanner module
"""

import datetime
import logging
import os

import attr
from scapy.all import Dot11, Dot11Beacon, Dot11ProbeReq, sniff

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)


@attr.s(frozen=True, cmp=False)
class ScanResult:
    """ Single scan result
    """
    mac = attr.ib(validator=attr.validators.instance_of(str))
    layer = attr.ib(validator=attr.validators.instance_of(str))
    datetime = attr.ib(validator=attr.validators.instance_of(str))
    ssid = attr.ib(validator=attr.validators.instance_of(str))
    chipset = attr.ib(validator=attr.validators.instance_of(str))

    def __eq__(self, other):
        return self.mac == other.mac

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.mac)

    def asdict(self):
        """ Return the current object in dict format
        """
        return attr.asdict(self)


@attr.s
class ScanContext:
    """ Scan context class
    """
    vendors_dict = attr.ib(validator=attr.validators.instance_of(dict))
    layers = attr.ib(validator=attr.validators.instance_of(list), factory=list)
    writers = attr.ib(validator=attr.validators.instance_of(list), factory=list)
    results = attr.ib(init=False, factory=set)


class WScanner:
    """ Wireless scanner class
    """
    def __init__(self, interface=None, timeout=None, vendors_path=None,
                 beacons_only=False, probes_only=False, writers=None,
                 **settings_override):
        self._interface = interface
        self._timeout = timeout

        layers = [Dot11Beacon, Dot11ProbeReq]
        if beacons_only:
            layers = [Dot11Beacon]
        if probes_only:
            layers = [Dot11ProbeReq]

        self._scan_ctxt = ScanContext(vendors_dict=_load_vendors(vendors_path),
                                      layers=layers, writers=writers)

    def scan(self):
        """ Start scanning
        """
        sniff(iface=self._interface,
              timeout=self._timeout,
              prn=lambda pkt: _packet_analyzer(pkt, self._scan_ctxt),
              store=0)

        return self._scan_ctxt.results


def _packet_analyzer(pkt, scan_ctxt):
    if pkt.haslayer(Dot11):
        for layer in scan_ctxt.layers:
            if pkt.haslayer(layer):
                scan_result = ScanResult(mac=pkt.addr2,
                                         layer=pkt.getlayer(layer).name,
                                         datetime=datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
                                         ssid=pkt.info.decode('utf-8'),
                                         chipset=_vendor_from_mac(pkt.addr2, scan_ctxt.vendors_dict))

                if scan_result not in scan_ctxt.results:
                    scan_ctxt.results.add(scan_result)
                    for writer in scan_ctxt.writers:
                        if callable(writer):
                            writer(scan_result)


def _vendor_from_mac(mac, vendors_dict):
    """ Try to retrieve the vendor designation of the mac address
    """
    oui = mac[0:8].upper()
    return vendors_dict.get(oui, '')


def _load_vendors(vendors_path):
    """ Create and return a dictionary of vendors OUI/NAME
    from a vendors file. The file shall contain a list of OUI \t NAME
    """
    vendors_dict = dict()

    if not vendors_path or not os.path.exists(vendors_path):
        return vendors_dict

    with open(vendors_path) as vendors:
        for line in vendors:
            line_split = line.split("\t")
            oui = line_split[0]
            name = line_split[1].rstrip()
            vendors_dict[oui] = name

    return vendors_dict
