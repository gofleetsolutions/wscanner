# -*- coding: utf-8 -*-
# Copyright 2018-2019 GO FLEET SOLUTIONS S.R.L. See LICENSE for details.
"""
wscanner entry point
"""

import argparse
import json
import os
import sys

import zmq

from . import __version__
from .wscanner import WScanner

VERSION_INTRO = r'wscanner v{}'.format(__version__)


def determine_path():
    try:
        root = __file__
        if os.path.islink(root):
            root = os.path.realpath(root)
        return os.path.dirname(os.path.abspath(root))
    except:
        print("Invalid __file__ variable.")
        sys.exit()


def parse_args():
    parser = argparse.ArgumentParser(description='Scan nearby wireless devices and publish to 0MQ sockets.')
    parser.add_argument('-i', '--interface', dest='interface', default='mon0',
                        help='Wireless interface in monitor mode (default=mon0).')
    parser.add_argument('-d', '--period', dest='period', type=int, default=10,
                        help='Scan period in <secs> (default=10).')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-b', '--beacons-only', dest='beacons_only', action='store_true',
                       help='Print beacons only (access points).')
    group.add_argument('-p', '--probes-only', dest='probes_only', action='store_true',
                       help='Print probe requests only (devices).')
    parser.add_argument('-vp', '--vendors-path', dest='vendors_path',
                        help='Explicitly set the vendors path.')
    parser.add_argument('-a', '--addr', dest='address', action='append', type=str,
                        help='0MQ socket address to publish data, in the form \'protocol://interface:port\'. '
                        'You may set this option multiple times.')
    parser.add_argument('-t', '--topic', dest='topic', default='WLS',
                        help='OMQ data topic (default=\'WLS\').')
    parser.add_argument('-q', '--quiet', dest='quiet', action='store_true',
                        help='Disable logging of scan results.')
    parser.add_argument('-V', '--version', dest='show_version', action='store_true')

    return vars(parser.parse_args())


class Publisher:
    def __init__(self, addrs, topic, encoding='utf-8'):
        self._context = zmq.Context()
        self._publisher = self._context.socket(zmq.PUB)
        self._topic = topic
        self._encoding = encoding

        if isinstance(addrs, str):
            addrs = (addrs,)

        for addr in addrs:
            self._publisher.bind(addr)

    def send(self, data):
        multipart_msg = [part.encode(self._encoding) for part in [self._topic, data]]
        self._publisher.send_multipart(multipart_msg)


def publish_writer(publisher):
    """ Create closure to publish scan result over ZeroMQ socket
    """
    def _wrapper(result):
        publisher.send(json.dumps(result.asdict()))

    return _wrapper


def main():
    arguments = parse_args()
    if arguments.get('show_version'):
        print(VERSION_INTRO)
        return

    if not arguments.get('vendors_path'):
        arguments['vendors_path'] = determine_path() + '/data/vendors.txt'

    topic = arguments.get('topic')
    addrs = arguments.get('address')
    publisher = None
    if addrs:
        publisher = Publisher(addrs, topic)

    period = arguments.pop('period')

    writers = [publish_writer(publisher)]
    if not arguments.pop('quiet', None):
        writers.append(lambda result: print(result.asdict()))

    while True:
        try:
            WScanner(writers=writers, timeout=period, **arguments).scan()

        except KeyboardInterrupt:
            break


if __name__ == '__main__':
    main()
