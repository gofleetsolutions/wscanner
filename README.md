# wscanner
Scan for nearby wireless devices and access-points.

Wscanner is Python3 Scapy project that scans for nearby wireless devices and publishes
results over a [ZeroMQ](http://zeromq.org/) socket in Json format.

# Installation

Clone the source tree:
```sh
git clone https://gitlab.com/gofleetsolutions/wscanner.git
```

Install wscanner with:
```sh
python3 setup.py install
```

# Wireless configuration

The wireless chipset shall support `monitor` mode.
Check that `monitor` is listed in 'Supported interface modes' section by running:
```sh
iw phy
```

Add a monitor interface named 'mon0':
```sh
iw phy phyX interface add mon0 type monitor
```

# Usage
From the command line, scanning over mon0 and publishing to ipc /tmp/wscanner:
```sh
wscanner --interface mon0 --addr 'ipc:///tmp/wscanner'
```

Or publishing over tcp (port 55555)
```sh
wscanner --interface mon0 --addr 'tcp://*:55555'
```

Scan for probe requests only (devices):
```sh
wscanner --interface mon0 --addr 'tcp://*:55555' --probes-only
```

Or for beacons only (access-points):
```sh
wscanner --interface mon0 --addr 'tcp://*:55555' --beacons-only
```

Publish with a custom topic:
```sh
wscanner --interface mon0 --addr 'ipc:///tmp/wscanner' --topic WDEV
```

Scanning is refreshed periodically, use a custom period (sec) with:
```sh
wscanner --interface mon0 --verbose --period 5
```

# How does wscanner work?

Wscanner is periodically sniffing for `Dot11` packets that contains `Beacon` and/or `Probe Request` layers.

Results are added in a `set` for a given period, means that the device is reported only once during the scan period.

Found devices/access-points mac addresses are stored as results along with the SSID, Chipset vendor information, layer type and datetime.

The chipset vendor is retrieved using an embedded OUI/vendor dataset.
Wscanner lets you provide your own custom dataset file containing an entry per line (OUI<tab>VENDOR).

Results are then published over a ZeroMQ socket in Json format:
```json
# Probe request from device
{
"mac": "68:e7:c2:aa:bb:cc",
"layer": "802.11 Probe Request",
"datetime": "2019-01-06T13:25:45Z",
"ssid": "Ssid1234",
"chipset": "Samsung Electronics Co.,Ltd"
}

# Beacon from Access-point
{
"mac": "cc:7b:35:aa:bb:cc",
"layer": "802.11 Beacon",
"datetime": "2019-01-06T13:25:45Z",
"ssid": "Ssid1234",
"chipset": "zte corporation"
}
```

# TODO list

* Include RadioTap information such as RSSI.

# Reports

* [Pylint report](https://gofleetsolutions.gitlab.io/wscanner/quality-report/pylint-report.html)
* [Coverage report](https://gofleetsolutions.gitlab.io/wscanner/coverage-report/index.html)
* [Unit tests report](https://gofleetsolutions.gitlab.io/wscanner/test-report/test_report.html)
